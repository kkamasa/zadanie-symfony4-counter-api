# README #

Zadanie polega na postawieniu prostej aplikacji na symfony4. 
Celem aplikacji ma byc zapisanie wartosci typu integer w bazie danych + możliwość modyfikacji tej wartości przez zapytania http

## Etap 1 ##

* Postawić docker za pomoca docker-compose (https://github.com/eko/docker-symfony)
* Zainstalować symfony4 (https://symfony.com/doc/current/setup.html) tak zeby było dostepne przez kontener dockera
* Zainstalować doctine i utworzyć nowe entity ktore bedzie przechowywać wartosć "countera"  (https://symfony.com/doc/current/doctrine.html)
* Utorzyć endpoint [POST] /modify ktory na celu ma zmodyfikować wartość "countera" o wartość podana w request (http://symfony.com/doc/current/controller.html) 

## Etap 2 ##

* Dodaj nowe Entity: ApiClient z jednym polem "token" typu string
* Dodaj nową relacje Many-to-One pomiędzy Counter(many) a ApiClient(one) https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/reference/association-mapping.html#many-to-one-unidirectional
* Dodaj ręcznie do bazy nowy ApiClient z polem token === 'TEST'
* Zainstaluj lokalnie program: https://www.getpostman.com/apps przyda sie on do testowania nowego endpointa
* Dodaj nowy endpoint [POST] /counter ma on dziłać tak: pobiera header z requestu o nazwie 'X-Token' i po nim wyszukuje w bazie danych ApiClient o podanym tokenie. Nastepnie tworzy nowy Counter z relacja do wyszukanego ApiClient i zwraca response Json {id: "id nowego Countera"}. Jesli ApiClient nie zostanie znaleziony endpont ma zwrocic 404.
* Dodaj nowy endpoint [GET] /counter ktory tez wyszuka ApiClient po 'X-Token' i zwroci przypisane do niego Counter`y jako list [{id: int, value: int}...]
* Zmień endpoint [GET] /modify na [POST] /counter/{id}/modify , zasada dzialania podoba z róznią: jesli counter o danym id nie istnije to 403, jesli 'X-Token' !== counter.apiClient.token to 401 - Access denied, jesli jest wszysko ok to 204 - No-content
### Na koniec ###

* Apka ma znajdować sie w tym repo
* Staraj się opisywać commity
* W razie problemów z dockerem możesz go olać i postawić lokalnie (ale wiadomo bedzie lepiej jak się nauczysz)
* Jeśli trafisz na bloker smiało pisz na slacku

Powodzenia! :)