<?php

namespace App\Controller;

use App\Entity\Counter;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CounterController extends Controller
{
    /**
     * @Route("/modify", name="modify")
     */
    public function modify(Request $request)
    {
        $counter = $this->getDoctrine()
            ->getRepository(Counter::class)
            ->find(1);

        if (!$counter) {
            $counter = new Counter();
        }

        $form = $this->createFormBuilder($counter)
            ->add('value', IntegerType::class)
            ->add('save', SubmitType::class, array('label' => 'Update'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->render('counter/form.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
